//
//  PIOViewController.m
//  ContourAnalize
//
//  Created by Admin on 10/31/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "PIOViewController.h"

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <complex.h>
#import <opencv2/highgui/cap_ios.h>

static  const int maxContourPoint = 30;

@interface PIOViewController ()
{
    
}
@property (strong, nonatomic) CvVideoCamera* videoCamera;

@property (weak, nonatomic) IBOutlet UIImageView *pictureImageView;
@property (weak, nonatomic) IBOutlet UIImageView *contourImageview;
@property (weak, nonatomic) IBOutlet UIImageView *equalizePicture;

- (cv::Mat)matWithImage:(UIImage *)image;
- (UIImage *)imageWithMat:(const cv::Mat&)cvMat;

- (std::vector < std::vector < std::complex <double>>>)preprocessing:(UIImage*)pictureImage;

- (cv::Mat)converToGray:(cv::Mat)mat;
- (cv::Mat)binarize:(cv::Mat)mat;

- (cv::Mat)processedImageByMaxGradient:(cv::Mat)mat;

- (void)calculateNormalizedScalarMultiplication:( std::vector < std::vector < std::complex <double>>>)vector_Contours secondVector:( std::vector < std::vector < std::complex <double>>>)vector_tamplate_Contours;

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size;

@end

static  NSString *fileName = @"numbers2.png";

@interface PIOViewController()
{
    std::vector < std::vector < std::complex <double>>> main_vector_tamplate_Contours;
    std::vector < std::vector < std::complex <double>>> main_vector_Contours;
}

@end

@implementation PIOViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.videoCamera = [[CvVideoCamera alloc] initWithParentView:_equalizePicture];
   
    self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionFront;
    self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset352x288;
    self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    self.videoCamera.defaultFPS = 1;
    self.videoCamera.grayscaleMode = NO;
    self.videoCamera.delegate = (id)self;
    [self.videoCamera start];
    
    std::vector < std::vector < std::complex <double>>> vector0;
    std::vector < std::vector < std::complex <double>>> vector1;
    std::vector < std::vector < std::complex <double>>> vector2;
    std::vector < std::vector < std::complex <double>>> vector3;
    std::vector < std::vector < std::complex <double>>> vector4;
    std::vector < std::vector < std::complex <double>>> vector5;
    std::vector < std::vector < std::complex <double>>> vector6;
    std::vector < std::vector < std::complex <double>>> vector7;
    std::vector < std::vector < std::complex <double>>> vector8;
    std::vector < std::vector < std::complex <double>>> vector9;
   
    vector0 = [self preprocessing:[UIImage imageNamed:@"alphaBet/0.png"]];
    vector1 = [self preprocessing:[UIImage imageNamed:@"alphaBet/1.png"]];
    vector2 = [self preprocessing:[UIImage imageNamed:@"alphaBet/2.png"]];
    vector3 = [self preprocessing:[UIImage imageNamed:@"alphaBet/3.png"]];
    vector4 = [self preprocessing:[UIImage imageNamed:@"alphaBet/4.png"]];
    vector5 = [self preprocessing:[UIImage imageNamed:@"alphaBet/5.png"]];
    vector6 = [self preprocessing:[UIImage imageNamed:@"alphaBet/6.png"]];
    vector7 = [self preprocessing:[UIImage imageNamed:@"alphaBet/7.png"]];
    vector8 = [self preprocessing:[UIImage imageNamed:@"alphaBet/8.png"]];
    vector9 = [self preprocessing:[UIImage imageNamed:@"alphaBet/9.png"]];
    

    
    main_vector_tamplate_Contours.push_back(std::vector<std::complex<double>>());
    main_vector_tamplate_Contours.push_back(std::vector<std::complex<double>>());
    main_vector_tamplate_Contours.push_back(std::vector<std::complex<double>>());
    main_vector_tamplate_Contours.push_back(std::vector<std::complex<double>>());
    main_vector_tamplate_Contours.push_back(std::vector<std::complex<double>>());
    main_vector_tamplate_Contours.push_back(std::vector<std::complex<double>>());
    main_vector_tamplate_Contours.push_back(std::vector<std::complex<double>>());
    main_vector_tamplate_Contours.push_back(std::vector<std::complex<double>>());
    main_vector_tamplate_Contours.push_back(std::vector<std::complex<double>>());
    main_vector_tamplate_Contours.push_back(std::vector<std::complex<double>>());
    
    main_vector_tamplate_Contours.at(0) = vector0.at(0);
    main_vector_tamplate_Contours.at(1) = vector1.at(0);
    main_vector_tamplate_Contours.at(2) = vector2.at(0);
    main_vector_tamplate_Contours.at(3) = vector3.at(0);
    main_vector_tamplate_Contours.at(4) = vector4.at(0);
    main_vector_tamplate_Contours.at(5) = vector5.at(0);
    main_vector_tamplate_Contours.at(6) = vector6.at(0);
    main_vector_tamplate_Contours.at(7) = vector7.at(0);
    main_vector_tamplate_Contours.at(8) = vector8.at(0);
    main_vector_tamplate_Contours.at(9) = vector9.at(0);
    
    
       

    
   // main_vector_Contours = [self preprocessing:[self imageWithImage:[UIImage imageNamed:fileName] convertToSize:CGSizeMake(2000, 800)]];
    
   // [self calculateNormalizedScalarMultiplication:main_vector_Contours secondVector:main_vector_tamplate_Contours];
 
 
    
   }

- (void)processImage:(cv::Mat&)image;
{
    main_vector_Contours = [self preprocessing:[self imageWithImage:[UIImage imageNamed:fileName] convertToSize:CGSizeMake(2000, 800)]];
    
    [self calculateNormalizedScalarMultiplication:main_vector_Contours secondVector:main_vector_tamplate_Contours];
   
}

- (void)calculateNormalizedScalarMultiplication:(std::vector < std::vector < std::complex <double>>>)vector_Contours secondVector:(std::vector < std::vector < std::complex <double>>>)vector_tamplate_Contours
{

    std::vector < std::vector < std::complex <double>>> vector_contour_ermit;
    
    
    #pragma mark создание эрмитово сопряженного Вектор Контура
    
    for (int j= 0; j<vector_tamplate_Contours.size(); j++)
    {
        vector_contour_ermit.push_back(std::vector<std::complex<double>>());
        
        for (int index = 0; index < maxContourPoint; index++)
        {
            vector_contour_ermit.at(j).push_back(std::complex<double>());
            
            vector_contour_ermit.at(j).at(index).real((vector_tamplate_Contours.at(j).at(index).real()));
            vector_contour_ermit.at(j).at(index).imag((vector_tamplate_Contours.at(j).at(index).imag() * -1));
        }
    }
    
    // ----------------------------->>>>>>>>>------>>>>>>----->>>>>>>>>-->>>->>>>GOOOOD UP

#pragma mark вычисление скалярного произведения Вектор Контуров MAX
    
    std::complex <double>elementScalarMultiplication = {0,0};
    
    std::vector< std::vector< std::complex<double>>> summaryScalarValue{0};
    
    
    
    std::vector<std::complex<double>> maxScalarValue = {0,0};
  
    
    for (int contoursIndex = 0; contoursIndex < vector_Contours.size(); contoursIndex++)
    {
        summaryScalarValue.push_back(std::vector<std::complex<double>>());
        
        for (int contoursTemplateindex = 0; contoursTemplateindex < vector_tamplate_Contours.size(); contoursTemplateindex++)
        {
            summaryScalarValue.at(contoursIndex).push_back(std::complex<double>());
           
            for (int j = 0; j < maxContourPoint; j++)
            {
                int valueFromOneToMax = 0;
               
                for (int index = j; index < maxContourPoint+j; index++)
                {
                    int  currentIndex = index;
                    
                    if (index >= maxContourPoint)
                    {
                        currentIndex = index - maxContourPoint;
                    }

                    elementScalarMultiplication += vector_Contours.at(contoursIndex).at(valueFromOneToMax) * vector_contour_ermit.at(contoursTemplateindex).at(currentIndex);
                    
                    valueFromOneToMax++;
                }
                
                if (abs(summaryScalarValue.at(contoursIndex).at(contoursTemplateindex)) < abs(elementScalarMultiplication))
                {
                    summaryScalarValue.at(contoursIndex).at(contoursTemplateindex) = elementScalarMultiplication;
                }
               
                elementScalarMultiplication = {0,0};
            }
        }
    }
    
    
#pragma mark вычисление нормы длины вектор - контура
    
    std::vector <std::complex<double>> moduleContours;
    std::vector <std::complex<double>> moduleTamplate;
    
    for (int j = 0; j < vector_Contours.size(); j++)
    {
        moduleContours.push_back(std::complex<double>());
        
        for (int index = 0; index < maxContourPoint; index++)
        {
            moduleContours.at(j) +=  pow(abs ( vector_Contours.at(j).at(index)), 2);
        }
    }
    
    for (int index = 0; index < moduleContours.size(); index++)
    {
        moduleContours.at(index) = sqrt(moduleContours.at(index));
    }

    
    for (int j = 0; j < vector_tamplate_Contours.size(); j++)
    {
        moduleTamplate.push_back(std::complex<double>());
        
        for (int index = 0; index < maxContourPoint; index++)
        {
           moduleTamplate.at(j) +=  pow(abs ( vector_tamplate_Contours.at(j).at(index)), 2);
        }
    }

    for (int index = 0; index < moduleTamplate.size(); index++)
    {
        moduleTamplate.at(index) = sqrt(moduleTamplate.at(index));
    }
    
    std::vector<std::complex<double>> RESULT;;
    std::complex<double>result = {0,0};
   
    std::vector<int> numberOfTemplate;
    
    for (int contoursIndex = 0; contoursIndex < moduleContours.size(); contoursIndex++)
    {
        RESULT.push_back(std::complex<double>());
        numberOfTemplate.push_back(int());
        
        for (int contoursTemplateIndex = 0; contoursTemplateIndex < moduleTamplate.size(); contoursTemplateIndex++)
        {
            result = summaryScalarValue.at(contoursIndex).at(contoursTemplateIndex) / moduleContours.at(contoursIndex)/moduleTamplate.at(contoursTemplateIndex);
            
            if (abs (RESULT.at(contoursIndex)) < abs (result) )
            {
                RESULT.at(contoursIndex) = result;
               
                numberOfTemplate.at(contoursIndex) = contoursTemplateIndex;
            }
            
        }
    }
    for (int i=0;i<RESULT.size();i++)
    {
        RESULT.at(i)=abs(RESULT.at(i));
    }
    
#pragma mark  олтображение результатов на  экране 
//----->>>>>>>>------>>>>>>>>>>>---->>>>---.>>>>>>>>>>>>>_---->_-_-__---------_-_-_-_-_-_-___-_-_-_-_--_-_------------__--__-___-__-___-_-___-__-_-_-_-_-____-__-_-_-____-_--
    
   
   // cv::Mat mat = [self matWithImage:[UIImage imageNamed:fileName]];
    cv::Mat mat = [self matWithImage:[self imageWithImage:[UIImage imageNamed:fileName] convertToSize:CGSizeMake(2000, 800)]];
    
    // cv::resize(mat, mat, cv::Size(8000, 8000));
    cv::Mat greyMat = [self converToGray:mat];
    cv::Mat binarizeMat =[self binarize:greyMat];
    
    cv::Mat grad = [self processedImageByMaxGradient:binarizeMat];  // пока что нужно что б было выключено
    
    grad =[self binarize:grad];
    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    
    cv::findContours( grad, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_TC89_KCOS);
    
   
    for ( size_t i=0; i<contours.size(); ++i )
    {
        cv::drawContours( grad, contours, i, cv::Scalar(255,0,0), 5, 8, hierarchy, 0, cv::Point() );
        cv::Rect brect = cv::boundingRect(contours[i]);
        
        cv::Point point = {brect.x,brect.y};
       
        std::stringstream ss;
        ss << numberOfTemplate.at(i);
        
        cv::putText(grad,ss.str(),point, cv::FONT_HERSHEY_COMPLEX_SMALL, 8, cvScalar(255,0,0), 6, CV_AA);
    }
    
    
    UIImage *image = [self imageWithMat:grad];
    [_pictureImageView setImage:image];
   
    [_contourImageview setImage:image];


    
    NSLog(@"");
}

#pragma mark   PRERPROCESSING -------->>>>>>----->>>>>>>

- (std::vector < std::vector < std::complex <double>>>)preprocessing:(UIImage*)pictureImage
{
    cv::Mat mat = [self matWithImage:pictureImage];
    
    if ((pictureImage.size.width<50)&&(pictureImage.size.height <50))
    {
     //   cv::resize(mat, mat, cv::Size(8000, 5000));
    }
    else
    {
    //    cv::resize(mat, mat, cv::Size(8000, 8000));
    }
        cv::Mat greyMat = [self converToGray:mat];
    cv::Mat binarizeMat =[self binarize:greyMat];
   
    cv::Mat grad = [self processedImageByMaxGradient:binarizeMat];  // пока что нужно что б было выключено
    
    grad =[self binarize:grad];
   
    
    cv::Mat gradCopy = grad.clone();
    
//    UIImage *image = [self imageWithMat:grad];
//    [_pictureImageView setImage:image];

    // нахождение контуров
    
    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
   
    cv::findContours( grad, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_TC89_KCOS);

#pragma mark drow contour
 
    for ( size_t i=0; i<contours.size(); ++i )
    {
        cv::drawContours( grad, contours, i, cv::Scalar(255,0,0), 3, 8, hierarchy, 0, cv::Point() );
      //  cv::Rect brect = cv::boundingRect(contours[i]);
       // cv::rectangle(grad, brect, cv::Scalar(160,0,0));
    }
#pragma mark замыкаем контурЫ на себя
    
    for ( size_t i=0; i < contours.size(); ++i )
    {
        contours.at(i).push_back(contours.at(i).at(0));
    }
#pragma mark ---------------
   
    std::cout << '\n';
   // std::cout << "main Contour" << contours.at(1);
   // std::cout << "\n \n";
    
    
    UIImage *imageContour = [self imageWithMat:grad];
    [_contourImageview setImage:imageContour];

    
#pragma mark преобразуем контур в контур с единичными векторами ЭВ
 
    std::vector<std::vector<cv::Point> >contourWithOnePointVector;
    for ( size_t i = 0;i < contours.size();i++)
    {
        contourWithOnePointVector.push_back(std::vector<cv::Point>());
    }
    
    for (int j = 0; j < contours.size(); j++)
    {
        for (int index = 0; index < contours.at(j).size() - 1; index++)
        {
            cv::Point pointFirst = contours.at(j).at(index);
            cv::Point pointSecond = contours.at(j).at(index+1);
            
            while ((pointFirst.x != pointSecond.x) || (pointFirst.y != pointSecond.y))
            {
                cv::Point point;
               
                if (pointSecond.x > pointFirst.x)
                {
                    point.x = pointFirst.x + 1;
                }
                else if (pointSecond.x < pointFirst.x)
                {
                    point.x = pointFirst.x - 1;
                }
                else
                {
                    point.x = pointFirst.x;
                }
               
                if (pointSecond.y > pointFirst.y)
                {
                    point.y = pointFirst.y + 1;
                }
                else if (pointSecond.y < pointFirst.y)
                {
                    point.y = pointFirst.y - 1;
                }
                else
                {
                    point.y = pointFirst.y;
                }

                pointFirst = point;
             
                contourWithOnePointVector.at(j).push_back(point);
            }
        }
    }
    
    std::cout << '\n';
    std::cout << '\n';
   // std::cout << "countour with one Point " <<' ' << contourWithOnePointVector.at(1);
    std::cout << '\n';
    std::cout << '\n';
    // нормирование контура по длине
    
    std::vector<std::vector<cv::Point> >equalizedContour;
    
     for (int i=0;i<contourWithOnePointVector.size();i++)
    {
        equalizedContour.push_back(std::vector<cv::Point>());
    }
    
    // ----->>>>_--->>>>_-->>>>> problem is under !!  this line ----->>>>------>>---->>--->
    
    for (int j = 0; j < equalizedContour.size(); j++)
    {
        for(int index = 0; index < maxContourPoint; index++)
        {
            cv::Point point = contourWithOnePointVector.at(j).at(contourWithOnePointVector.at(j).size() / maxContourPoint*index);
            
            equalizedContour.at(j).push_back(point);
        }
    }
        //  std::cout << "CountourNEW!!! " <<' '<<equalizedContour.at(0);
   
    // ----->>>>_--->>>>_-->>>>> problem is upper !!  this line ----->>>>------>>---->>--->


    // отрисовка  эквализированного контура

    
   
    for ( size_t i = 0; i < equalizedContour.size(); ++i )
    {
        cv::drawContours(grad, equalizedContour, i, cv::Scalar(255,0,0), 1, 8, hierarchy, 0, cv::Point());
    }
    
   // UIImage *imageEqualize = [self imageWithMat:grad];
    //[_equalizePicture setImage:imageEqualize];

    
    // создание Вектор Контура  (кодирование)
   
  
    std::vector < std::vector < std::complex <double>>> vector_Contour;
    
    for (int j = 0; j < equalizedContour.size(); j++)
    {
        vector_Contour.push_back(std::vector<std::complex<double>>());
    }
    
    
    for (int j = 0; j < equalizedContour.size(); j++)
    {
        cv::Point firstElement = equalizedContour.at(j).at(0);
     
        for (int index = 0; index < equalizedContour.at(j).size(); index++)
        {
            if (index != equalizedContour.at(j).size() - 1)
            {
                std::complex<double> point = {equalizedContour.at(j).at(index + 1).x - equalizedContour.at(j).at(index).x,  equalizedContour.at(j).at(index + 1).y - equalizedContour.at(j).at(index).y};
                vector_Contour.at(j).push_back(point);
            }
            else
            {
                std::complex<double> point = {equalizedContour.at(j).at(0).x - equalizedContour.at(j).at(index).x,  equalizedContour.at(j).at(0).y - equalizedContour.at(j).at(index).y};
                vector_Contour.at(j).push_back(point);
            }
        }
    }
    
    for (int j = 0; j < vector_Contour.size(); j++)
    {
        for (int index = 0; index < vector_Contour.at(j).size(); index++)
        {
            //std::cout << vector_Contour.at(j).at(index);
        }
        std::cout <<"\n\n\n\n";
    }
    
    return vector_Contour;
}
- (cv::Mat)converToGray:(cv::Mat)mat
{
    cv::Mat grayMat;
    cv::cvtColor(mat, grayMat, CV_BGRA2GRAY);
    return grayMat;
}

- (cv::Mat)binarize:(cv::Mat)mat
{
    cv::Mat binarizeMat;
    cv::threshold(mat, binarizeMat, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
    return binarizeMat;
}

- (cv::Mat)processedImageByMaxGradient:(cv::Mat)mat
{
    int ddepth = CV_16S;
    int scale = 1;
    int delta = 0;
    cv::Mat grad;

    cv::Mat grad_x, grad_y;
    cv::Mat abs_grad_x, abs_grad_y;

   
    cv::Scharr( mat, grad_x, ddepth, 1, 0, scale, delta, cv::BORDER_DEFAULT );
    convertScaleAbs( grad_x, abs_grad_x );
    Scharr( mat, grad_y, ddepth, 0, 1, scale, delta, cv::BORDER_DEFAULT );
    convertScaleAbs( grad_y, abs_grad_y );
    cv::addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );

    return grad;
}


- (cv::Mat)matWithImage:(UIImage *)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    
    CGFloat cols = image.size.width; // or height
    CGFloat rows = image.size.height;// or width
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to backing data
                                                    cols,                      // Width of bitmap
                                                    rows,                     // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}

- (UIImage *)imageWithMat:(const cv::Mat&)cvMat
{
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize() * cvMat.total()];
    
    CGColorSpaceRef colorSpace;
    
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    }
    else
    {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                     // Width
                                        cvMat.rows,                                     // Height
                                        8,                                              // Bits per component
                                        8 * cvMat.elemSize(),                           // Bits per pixel
                                        cvMat.step[0],                                  // Bytes per row
                                        colorSpace,                                     // Colorspace
                                        kCGImageAlphaNone | kCGBitmapByteOrderDefault,  // Bitmap info flags
                                        provider,                                       // CGDataProviderRef
                                        NULL,                                           // Decode
                                        false,                                          // Should interpolate
                                        kCGRenderingIntentDefault);                     // Intent
    
    UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    
    return image;
}
- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}
@end
